% investigate stimulus update frequency based on experimental protocol

clear all; clc;

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/';
pn.dataIn   = [pn.root, 'data/behavior/STSW_dynamic/E_stimJitter/B_data/A_rawData/'];
pn.dataOut   = [pn.root, 'data/behavior/STSW_dynamic/E_stimJitter/B_data/B_stimulusTiming/']; mkdir(pn.dataOut);
pn.plotFolder = [pn.root, 'data/behavior/STSW_dynamic/E_stimJitter/C_figures/'];

%% get available behavioral subjects

ID_MRI = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';...
    '1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';...
    '1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';...
    '1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';...
    '1266';'1268';'1270';'1276';'1281';'2104';'2107';'2108';'2112';'2118';...
    '2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';...
    '2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';...
    '2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';...
    '2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';...
    '2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

ID_EEG = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1138';'1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';...
    '1173';'1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';'2104';'2107';...
    '2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';...
    '2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';...
    '2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

IDs_Complete = intersect(ID_MRI,ID_EEG);

%% calculate update frequency for EEG & MRI session

trig.eventTriggers = {'StimOnset'; 'RDMUpdate'; 'RespOnset'};

for indID = 1:numel(IDs_Complete)
    disp(['Working on subject ', IDs_Complete{indID}]);
    fileEEG = dir([pn.dataIn, IDs_Complete{indID}, '_StateSwitch_dynamic*']);
    fileMRI = dir([pn.dataIn, IDs_Complete{indID}, '_StateSwitchMR_dynamic*']);
    ProtocolEEG = load([fileEEG(1).folder, '/', fileEEG(1).name], 'ExperimentProtocol');
    ProtocolMRI = load([fileMRI(1).folder, '/', fileMRI(1).name], 'ExperimentProtocol');
    
    % get stimulus updates (256 trials each): EEG
    evt = ProtocolEEG.ExperimentProtocol(:,1);
    evt_time = ProtocolEEG.ExperimentProtocol(:,2);
    eventsEEG{indID} = NaN(64,100);
    indOnset = find(strcmp(evt,trig.eventTriggers{1}));
    indOffset = find(strcmp(evt,trig.eventTriggers{3}));
    for indTrial = 1:numel(indOnset)
       indStims = find(strcmp(evt(indOnset(indTrial):indOffset(indTrial),1),...
           trig.eventTriggers{2}));
       targetSamples = [evt_time(indOnset(indTrial)+indStims-1)];
       eventsEEG{indID}(indTrial, 1:numel(targetSamples)) = cell2mat(targetSamples);
    end
    eventsEEGdiff{indID} = diff(eventsEEG{indID},[],2);
    
    % % get stimulus updates (256 trials each): MRI
    evt = ProtocolMRI.ExperimentProtocol(:,1);
    evt_time = ProtocolMRI.ExperimentProtocol(:,2);
    eventsMRI{indID} = NaN(64,100);
    indOnset = find(strcmp(evt,trig.eventTriggers{1}));
    indOffset = find(strcmp(evt,trig.eventTriggers{3}));
    for indTrial = 1:numel(indOnset)
       indStims = find(strcmp(evt(indOnset(indTrial):indOffset(indTrial),1),...
           trig.eventTriggers{2}));
       targetSamples = [evt_time(indOnset(indTrial)+indStims-1)];
       eventsMRI{indID}(indTrial, 1:numel(targetSamples)) = cell2mat(targetSamples);
    end
    eventsMRIdiff{indID} = diff(eventsMRI{indID},[],2);
end

%% save data

save([pn.dataOut, 'A_stimUpdateFromExperimentProtocols.mat'], 'eventsEEG', 'eventsEEGdiff', 'eventsMRI', 'eventsMRIdiff');

%% plot stimulus update frequency across subjects

eventsEEGCat = [];
for indID = 1:numel(IDs_Complete)
    if ~isempty(eventsEEG{indID})
        eventsEEGCat = [eventsEEGCat, reshape(eventsEEG{indID}', 1, [])];
    end
end
eventsEEGCat(isnan(eventsEEGCat)) = [];

eventsEEGdiffCat = [];
for indID = 1:numel(IDs_Complete)
    if ~isempty(eventsEEGdiff{indID})
        eventsEEGdiffCat = [eventsEEGdiffCat, reshape(eventsEEGdiff{indID}', 1, [])];
    end
end
eventsEEGdiffCat(isnan(eventsEEGdiffCat)) = [];
eventsEEGdiffCat(abs(eventsEEGdiffCat)<10^-3) = [];
eventsEEGdiffCat(abs(eventsEEGdiffCat)>300) = [];

h = figure('units','normalized','position',[0 0 .7 1]);
subplot(3,1,1); plot(eventsEEGdiffCat); xlim([1, size(eventsEEGdiffCat,2)]); ylim([.032 .035]);
xlabel('Trial (concatenated across subjects)'); ylabel('Difference between updates (in ms)');
title('Difference between updates was supposed to be 30 Hz (33 ms difference between updates)')
subplot(3,1,2); plot(eventsEEGdiffCat(1:1000)); xlim([1, 1000]);
xlabel('Trial (concatenated across subjects)'); ylabel('Difference between updates (in ms)');
title('YA have encoding of stimulus triggers @ 30 Hz')
subplot(3,1,3); plot(eventsEEGdiffCat(8*10^5:8*10^5+1000)); xlim([1, 1000])
xlabel('Trial (concatenated across subjects)'); ylabel('Difference between updates (in ms)');
title('OA have encoding of stimulus triggers @ 30 Hz')

suptitle('Trigger jitter concatenated across subjects (first YA, then OA)');
set(findall(gcf,'-property','FontSize'),'FontSize',15)

figureName = 'A_stimUpdate';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
