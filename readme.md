[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Check stimulus presentation jitter

- stimulus update frequency should be 30 Hz
- check how often frames are missed

![stimUpdate](figures/A_stimUpdate.png)